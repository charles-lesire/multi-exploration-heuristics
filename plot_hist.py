import pylab
import numpy as np
import operator

from plot_data import instance_id_from_string

def parse_data(data, cols={'bnc': 6, 'tp': 10, 'mp': 14}, limit=60):
    r = []
    for row in data:
        d = {k: np.inf for k in cols.keys()}
        for method, col in cols.items():
            if row[col+1] <= limit:
                d[method] = row[col]
            else:
                d[method] = np.nan
        d["T"] = row[1]
        r.append(d)

    result = {k: {kk: [0, 0, 0] for kk in cols.keys()} for k in [2,5,10,20]}
    for l in r:
        best = np.nanmin([l['bnc'], l['tp'], l['mp']])
        for m in cols.keys():
            obj = l[m]
            if obj == best:
                result[l['T']][m][0] += 1
            elif obj <= 1.05*best:
                result[l['T']][m][1] += 1
            elif not np.isnan(obj):
                result[l['T']][m][2] += 1
    return result

def plot_hist(obj, filename):
    width = .2
    xs = sorted([k for k in obj.keys()])
    xn = range(len(xs))

    best = [obj[k]['bnc'][0] for k in xs]
    near = [obj[k]['bnc'][1] for k in xs]
    solved = [obj[k]['bnc'][2] for k in xs]
    pylab.bar([x-2*width for x in xn], best, width, color='#7B241C', ecolor='#641E16')
    pylab.bar([x-2*width for x in xn], near, width, bottom=best, hatch='/', color='#A93226', ecolor='#641E16')
    pylab.bar([x-2*width for x in xn], solved, width, bottom=[near[x] + best[x] for x in xn], color='#CD6155', ecolor='#641E16', label='Branch&Cut')

    best = [obj[k]['tp'][0] for k in xs]
    near = [obj[k]['tp'][1] for k in xs]
    solved = [obj[k]['tp'][2] for k in xs]
    pylab.bar([x-.5*width for x in xn], best, width, color='#21618C', ecolor='#1B4F72')
    pylab.bar([x-.5*width for x in xn], near, width, bottom=best, hatch='/', color='#2E86C1', ecolor='#1B4F72')
    pylab.bar([x-.5*width for x in xn], solved, width, bottom=[near[x] + best[x] for x in xn], color='#5DADE2', ecolor='#1B4F72', label='Two-phase')

    best = [obj[k]['mp'][0] for k in xs]
    near = [obj[k]['mp'][1] for k in xs]
    solved = [obj[k]['mp'][2] for k in xs]
    pylab.bar([x+width for x in xn], best, width, color='#196F3D', ecolor='#145A32')
    pylab.bar([x+width for x in xn], near, width, bottom=best, hatch='/', color='#229954', ecolor='#145A32')
    pylab.bar([x+width for x in xn], solved, width, bottom=[near[x] + best[x] for x in xn], color='#52BE80', ecolor='#145A32', label='Multiphase')

    pylab.xlabel('number of robots')
    pylab.ylabel('number of instances solved')
    pylab.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left",
                mode="expand", borderaxespad=0, ncol=3)
    pylab.xticks(xn, xs)
    #pylab.show()
    pylab.savefig(filename)
    pylab.clf()

if __name__ == '__main__':
    import pandas
    data = pandas.read_excel("summary.xlsx", sheet_name="summary", header=[1,2,3], na_values=["#N/D", "na"],
        converters={0: instance_id_from_string})
    print(data.values)

    # time limit 60
    obj = parse_data(data.values, cols={'bnc': 5, 'tp': 9, 'mp': 13})
    plot_hist(obj, 'hist-60.png')
    # time limit 600
    obj = parse_data(data.values, cols={'bnc': 7, 'tp': 11, 'mp': 15}, limit=600)
    plot_hist(obj, 'hist-600.png')
