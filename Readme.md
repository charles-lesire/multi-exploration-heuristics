## Dependencies

```
pip3 install xlrd
pip3 install pandas
```

## Generate figs

```
python3 plot_data.py
python3 plot_hist.py
```
