import pylab
import numpy as np
import operator

def parse_data(data, cols={'bnc': 6, 'tp': 10, 'mp': 14}, limit=60):
    instances = set([data[x][0] for x in range(len(data))])
    obj = {key: None for key in instances}
    cpu = {key: None for key in instances}
    for k in instances:
        obj[k] = {k: [] for k in cols.keys()}
        cpu[k] = {k: [] for k in cols.keys()}
    for row in data:
        for method, col in cols.items():
            c = row[col+1]
            if c <= limit:
                obj[row[0]][method].append(row[col])
                cpu[row[0]][method].append(c)
    return obj, cpu


def sort_data(instances, data, method='tp'):
    instances = sorted(list(instances))
    means = {key: None for key in instances}
    # tp
    for i in instances:
        d = [x for x in data[i][method] if not np.isnan(x)]
        if len(d) > 0:
            means[i] = np.sum(d)/len(d)
        else:
            means[i] = np.inf
    return sorted(means.items(), key=operator.itemgetter(1))

def plot_solved(instances, obj, filename, ylabel, sorted_means, ticks):
    methods=['tp', 'bnc', 'mp']
    solved = {key: None for key in methods}
    for m in methods:
        solved[m] = {key: None for key in instances}
        for i in instances:
            d = [x for x in obj[i][m] if not np.isnan(x)]
            solved[m][i] = len(d)
    pylab.figure(figsize=(12,6))
    width = .2
    ind = np.arange(len(sorted_means))
    pylab.bar(ind, [solved['tp'][x[0]] for x in sorted_means], width, color='blue', label='Two-phase')
    pylab.bar(ind+1.5*width, [solved['mp'][x[0]] for x in sorted_means], width, color='green', label='Multiphase')
    pylab.bar(ind+3*width, [solved['bnc'][x[0]] for x in sorted_means], width, color='red', label='Branch&Cut')
    pylab.ylim(0,12)
    pylab.xlabel('instance')
    pylab.xticks(range(len(sorted_means)), ticks, rotation='vertical')
    pylab.ylabel(ylabel)
    pylab.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left",
                mode="expand", borderaxespad=0, ncol=3)
    pylab.savefig(filename, bbox_inches='tight')
    pylab.clf()

def plot_data(instances, obj, filename, ylabel, sorted_means, ticks):
    instances = sorted(list(instances))
    means = {key: None for key in instances}
    mins = {key: None for key in instances}
    maxs = {key: None for key in instances}
    # tp
    for i in instances:
        d = [x for x in obj[i]['tp'] if not np.isnan(x)]
        if len(d) > 0:
            means[i] = np.sum(d)/len(d)
            mins[i] = np.min(d)
            maxs[i] = np.max(d)
        else:
            means[i] = np.inf
            mins[i] = np.inf
            maxs[i] = np.inf
    #pylab.errorbar(range(len(sorted_means)), [means[x[0]] for x in sorted_means], yerr=[stds[x[0]] for x in sorted_means], fmt='bo-', label='Two-phase')
    pylab.figure(figsize=(12,6))
    pylab.plot(range(len(sorted_means)), [means[x[0]] for x in sorted_means], 'bo-', label='Two-phase')
    pylab.fill_between(range(len(sorted_means)), [mins[x[0]] for x in sorted_means], [maxs[x[0]] for x in sorted_means], alpha=.2, facecolor='b')
    # mp
    for i in instances:
        d = [x for x in obj[i]['mp'] if not np.isnan(x)]
        if len(d) > 0:
            means[i] = np.sum(d)/len(d)
            mins[i] = np.min(d)
            maxs[i] = np.max(d)
        else:
            means[i] = np.inf
            mins[i] = np.inf
            maxs[i] = np.inf
    #pylab.errorbar(range(len(sorted_means)), [means[x[0]] for x in sorted_means], yerr=[stds[x[0]] for x in sorted_means], fmt='gs-', label='Multiphase')
    pylab.plot(range(len(sorted_means)), [means[x[0]] for x in sorted_means], 'gs-', label='Multiphase')
    pylab.fill_between(range(len(sorted_means)), [mins[x[0]] for x in sorted_means], [maxs[x[0]] for x in sorted_means], alpha=.2, facecolor='g')
    # bnc
    for i in instances:
        d = [x for x in obj[i]['bnc'] if not np.isnan(x)]
        if len(d) > 0:
            means[i] = np.sum(d)/len(d)
            mins[i] = np.min(d)
            maxs[i] = np.max(d)
        else:
            means[i] = np.inf
            mins[i] = np.inf
            maxs[i] = np.inf
    #pylab.errorbar(range(len(sorted_means)), [means[x[0]] for x in sorted_means], yerr=[stds[x[0]] for x in sorted_means], fmt='r^-', label='Branch&Cut')
    pylab.plot(range(len(sorted_means)), [means[x[0]] for x in sorted_means], 'r^-', label='Branch&Cut')
    pylab.fill_between(range(len(sorted_means)), [mins[x[0]] for x in sorted_means], [maxs[x[0]] for x in sorted_means], alpha=.2, facecolor='r')

    pylab.xlabel('instance')
    pylab.xticks(range(len(sorted_means)), ticks, rotation='vertical')
    pylab.ylabel(ylabel)
    pylab.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left",
                mode="expand", borderaxespad=0, ncol=3)
    #pylab.show()
    pylab.savefig(filename, bbox_inches='tight')
    pylab.clf()

def instance_id_from_string(s):
    import parse
    #t2_o25_n50_rep0
    parsed = parse.parse("t{}_o{}_n{}_rep{}", s)#.decode('utf-8'))
    t, o, n, rep = parsed
    return int(t)*1000+int(o)*10+(int(n)/10)

if __name__ == '__main__':
    import pandas
    data = pandas.read_excel("summary.xlsx", sheet_name="summary", header=[1,2,3], na_values=["#N/D", "na"],
        converters={0: instance_id_from_string})

    # time limit 60
    obj, cpu = parse_data(data.values, cols={'bnc': 5, 'tp': 9, 'mp': 13}, limit=60)
    sorted_instances = sort_data(obj.keys(), obj, 'tp')
    ticks = []
    for i in sorted_instances:
        for row in data.values:
            if row[0] == i[0]:
                break
        label = "T{:d}_O{:d}_N{:d}".format(int(row[1]), int(row[2]), int(row[3]))
        ticks.append(label)

    plot_data(obj.keys(), obj, 'objectives-60.png', 'objective', sorted_instances, ticks)
    plot_data(cpu.keys(), cpu, 'cpu-60.png', 'cpu time', sorted_instances, ticks)
    plot_solved(cpu.keys(), cpu, 'solved-60.png', 'solved instances', sorted_instances, ticks)

    # time limit 600
    obj, cpu = parse_data(data.values, cols={'bnc': 7, 'tp': 11, 'mp': 15}, limit=600)
    #sorted_instances = sort_data(cpu.keys(), cpu, 'tp')
    plot_data(obj.keys(), obj, 'objectives-600.png', 'objective', sorted_instances, ticks)
    plot_data(cpu.keys(), cpu, 'cpu-600.png', 'cpu time', sorted_instances, ticks)
    plot_solved(cpu.keys(), cpu, 'solved-600.png', 'solved instances', sorted_instances, ticks)
